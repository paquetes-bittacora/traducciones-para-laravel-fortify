<?php

declare(strict_types=1);

namespace Bittacora\FortifyTranslations;

use Bittacora\FortifyTranslations\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

class FortifyTranslationsProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->commands([
            InstallCommand::class
        ]);
    }
}
