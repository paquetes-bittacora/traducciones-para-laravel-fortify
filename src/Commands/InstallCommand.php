<?php

declare(strict_types=1);

namespace Bittacora\FortifyTranslations\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;

class InstallCommand extends Command
{
    protected $signature = 'fortify-translations:install';

    protected $description = 'Instala las traducciones de Laravel Fortify';
    private $filePath;

    /**
     * @throws JsonException
     */
    public function handle()
    {
        $this->filePath = App::langPath('es.json');

        if (file_exists($this->filePath)) {
            $fileContents = file_get_contents($this->filePath);
            $translations = json_decode($fileContents, true, 512, JSON_THROW_ON_ERROR);
            $this->addTranslations($translations);
        }
    }

    /**
     * @throws JsonException
     */
    private function addTranslations(array $translations): void
    {
        if (!array_key_exists('The :attribute must be at least :length characters.', $translations)) {
            $translations = array_merge($translations, $this->getTranslationsArray());
            $jsonData = json_encode($translations, JSON_THROW_ON_ERROR|JSON_PRETTY_PRINT);
            file_put_contents($this->filePath, $jsonData);
        }
    }

    private function getTranslationsArray(): array
    {
        return [
            "The :attribute must be at least :length characters and contain at least one uppercase character." => "El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula.",
            "The :attribute must be at least :length characters and contain at least one number." => "El :attribute debe tener al menos :length caracteres y contener al menos un número.",
            "The :attribute must be at least :length characters and contain at least one special character." => "El :attribute debe tener al menos :length caracteres y contener al menos un símbolo.",
            "The :attribute must be at least :length characters and contain at least one uppercase character and one number." => "El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula y un número.",
            "The :attribute must be at least :length characters and contain at least one uppercase character and one special character." => "El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula y un símbolo.",
            "The :attribute must be at least :length characters and contain at least one uppercase character, one number, and one special character." => "El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula, un número, y un símbolo.",
            "The :attribute must be at least :length characters and contain at least one special character and one number." => "El :attribute debe tener al menos :length caracteres y contener al menos un símbolo y un número.",
            "The :attribute must be at least :length characters." => "El :attribute debe tener al menos :length caracteres."
        ];
    }
}
