# Traducciones para Laravel Fortify

Este paquete instala las traducciones para algunas cadenas de Fortify que
tienen que registrarse en `resources/lang/es.json`, y que no pueden registrarse
desde un paquete de la forma tradicional.

Para instalar las traducciones simplemente hay que ejecutar el comando:

```
php artisan fortify-translations:install
```
